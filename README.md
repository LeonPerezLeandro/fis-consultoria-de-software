# MAPA 1 - La consultoría.
```plantuml
@startmindmap
*[#Orange] La consultoría. Una profesión\nde éxito presente y futuro
 *[#lightgreen] ¿Qué es la consultoría?
  *[#FFBBCC] Nace
   *[#lightblue] Ya que existen grandes organizaciones que\nson típicamente los clientes de las consultoras\nque internamente tienen mucha complejidad en\nsus procesos internos.
    *[#lightgreen] Requieren sistemas de\ninformación que les den\nsoporte a esos procesos\nque son complejos.

  *[#FFBBCC] En general
   *[#lightblue] Las empresas no pueden operar y\ntransformarse al mismo tiempo:
    *[#lightgreen] *Falta de personal cualificado
    *[#lightgreen] *Falta de conocimiento especifico
    *[#lightgreen] *Falta de tiempo

  *[#FFBBCC] Consulting
   *[#lightblue] Empresa de servicios profesionales\nde alto valor añadido
    *[#lightgreen] +Conocimiento sectorial
    *[#lightgreen] +Conocimiento técnico 
    *[#lightgreen] +Capacidad de plantear soluciones 
  *[#FFBBCC] Ejemplo
   *[#lightblue] AXPE Consulting 
    *[#lightgreen] Es una compañia multinacional, de capital 100%\nespañol, de Consultoria y Tecnologias de información,\nespecializada en Servicios de Outsourcing, Consultoria\ny Ejecucion de proyectos cerrrados.
    *[#lightgreen] Nacida en 1998 en España.
    *[#lightgreen] Compañia independiente, con gran solidez financiera,\nque da servicio a las compañias del Ibex35 y que\ndistribuye su facturacion entre mas de 150 clientes.
    *[#lightgreen] Más de 1600 profesiones a nivel mundial trabajan en:
     *[#lightgreen] Europa: 1300 empleados en\nEspaña (7 centros), UK y Francia
     *[#lightgreen] LATAM:
      *[#lightgreen] México con un equipo de 220 profesionales
      *[#lightgreen] Con presencia en Perú, Colombia, Brasil,\nPanamá, República Dominicana y Argentina

 *[#lightgreen] Tipos de servicios

  *[#FFBBCC] Consultoría
   *[#lightblue] Reingeniería de procesos
    *[#lightgreen] Ayudar a las empresas a que organicen sus actividades siguiendo\nun determinado esquema de funcionamiento de manera que\nsea mas optimo que tienen en la actualidad atendiendo a los cambios. 
   *[#lightblue] Gobierno TI
    *[#lightgreen] Hacen referencia sobre estandares y practicas \n que son guias donde requieren de implementación \n del mundo real de las empresas
   *[#lightblue] Oficina de proyectos
    *[#lightgreen] Son proyectos complejos hay muchos intervinientes\ny que casi siempre tiene involucración de un área\nIT y la gestión de los proyectos es realizada\nen la mayoría con una metodología adecuada.
   *[#lightblue] Analítica avanzada de datos
    *[#lightgreen] Hoy en día la información es crucial para las empresas,\n es muy importante que dispongan en tiempo real\nla información que les permita tomar decisiones.

  *[#FFBBCC] Integración
   *[#lightblue] Toda empresa tiene un mapa de sistemas que da soporte a\nsu utilidad de negocio y ese mapa de sistemas habla de piezas\nsoftware y piezas hardware.El mapa requiere evolución continua.
   *[#lightblue] Desarrollo a media
   *[#lightblue] Aseguramiento de la calidad E2E
    *[#lightgreen] Tiene repartido un mapa de sistemas en diferentes grupos\nde responsabilidad y es muy difícil que exista una visión de\nextremo a extremo. Los procesos de negocios suelen tener\nla mala costumbre de ser de extremo a extremo y por\ntanto tienen que intervenir en su solución.
   *[#lightblue] Infraestructuras
    *[#lightgreen] Las nuevas tecnologías influyen Es muy común que las empresas\nsuelen desarrollar sus sistemas de soporte a operaciones\ny negocios basándose en un software preconstruidos  
   *[#lightblue] Soluciones de mercado
    *[#lightgreen] Requieren un conocimiento profundo de la propia solución\npara entender lo que demanda el cliente y trasladar esa\nparametrización a los sistemas de soluciones para\nque nos dé una solución más viable.

  *[#FFBBCC] Externalización
   *[#lightblue] Gestión de aplicaciones
    *[#lightgreen] Las empresas deciden delegar todo el mantenimiento y\nevolución de los sistemas en un tercero, lo regula por un\nacuerdo a nivel de servicios parametrizado por complejidades\ny por diferentes tipos de medidas y lo establece por varios años.
   *[#lightblue] Servicios SQA
   *[#lightblue] Operación y administración\nde infraestructura
    *[#lightgreen] Área de crecimiento importante, de algunas empresas\nque deciden externalizar procesos completos de negocios\nasumiendo la operación
   *[#lightblue] Procesos de negocio

  *[#FFBBCC] Social
   *[#lightblue] Consultoría
    *[#lightgreen] SentimentalAnalysis
    *[#lightgreen] Definicion de mejoras de utilizacion de canales de atencion por redes rociales
   *[#lightblue] Integración
    *[#lightgreen] Desarrollo de conectores con redes sociales
   *[#lightblue] Externalización
    *[#lightgreen] Operación Comunity Manager

  *[#FFBBCC] Movilidad
   *[#lightblue] Consultoría
    *[#lightgreen] Definicion de practicas de Gobierno TI para Mobile.
    *[#lightgreen] Simplificación de procesos con movilidad
   *[#lightblue] Integración
    *[#lightgreen] Movilización de aplicaciones existentes
     *[#lightgreen] Bootstrap
     *[#lightgreen] Cordova
    *[#lightgreen] Desarrollo y Prueba de APPS
    *[#lightgreen] Integración de soluciones
     *[#lightgreen] Firma biométrica
     *[#lightgreen] SMS /mail certificados
   *[#lightblue] Externalización
    *[#lightgreen] Operación de plataformas de aplicaciones móviles

  *[#FFBBCC] Analytics
   *[#lightblue] Consultoría
    *[#lightgreen] Definición de Arquitectura BIGDATA
    *[#lightgreen] Desarrollo de modelos analítics avanzados
   *[#lightblue] Integración
    *[#lightgreen] Integración de fuentes
    *[#lightgreen] Desarrollo de CdM
    *[#lightgreen] Integración de entornos relacionales y BIGDATA
   *[#lightblue] Externalización
    *[#lightgreen] Factoría de explotación y mantenimiento de modelos analíticos

  *[#FFBBCC] Cloud
   *[#lightblue] Consultoría
    *[#lightgreen] Definición de estrategia Cloud
    *[#lightgreen] Definicion de procedimientos de Operación Cloud
   *[#lightblue] Integración
    *[#lightgreen] Implantación de estrategias CLOUD
    *[#lightgreen] Desarrollo de herramientas de provisión CLOUD
    *[#lightgreen] Desarrollo de aplicaciones CLOUD
   *[#lightblue] Externalización
    *[#lightgreen] Operación de Servicios Cloud

 *[#lightgreen] ¿Porqué es una\nprofesión de futuro?
  *[#FFBBCC] En la sociedad del conocimiento, la consultoria,\nes un modo de vida que permite alcanzar\nla felicidad profesional
   *[#lightblue] Felicidad profesional =\nRetribución competivida +\nCarrera profesional +\nReto
  *[#FFBBCC] El mercado actual
   *[#lightblue] El PIB creció un 3,2% en 2015.\nEl Sector crece por encima del PIB 
    *[#lightblue] Ingresos 
     *[#lightgreen] 11.270 millonres\n de euros en 2015
      *[#lightgreen] Mercado español\n8.145 millones
      *[#lightgreen] Mercado exterior\n3.125 millones
     *[#lightgreen] Las ventas crecen:\n5,1% respecto a 2014

    *[#lightblue] Distribución de ingresos 
     *[#lightgreen] Por servicios
      *[#lightgreen] Outsourcing\n46,0%
      *[#lightgreen] Desarrollo e integración\n37,8%
      *[#lightgreen] Consultorio\n16,2%
     *[#lightgreen] Principales\nindustrias
      *[#lightgreen] Servicios industriales\n29,0%
      *[#lightgreen] Administraciones Públicas\n15,8%
      *[#lightgreen] Telecomunicaciones\n13,2%     

   *[#lightblue] Empleo 
    *[#lightgreen] 144.265 profesionales\nen 2015        
    *[#lightgreen] El empleo aumenta:\n3,4% respecto\na 2014
    
  *[#FFBBCC] Tendencias
   *[#lightblue] La consultoría tiene un papael clave como proveedor\nde servicios de habliticacion digital.
    *[#lightgreen] Los habilitadores digitales tienen un papel clave\nen las grandes iniciativias de futuro que\nse marcan como tendencias:
     *[#lightgreen] Transformación Digital
     *[#lightgreen] Industria conectada 4.0
     *[#lightgreen] Smart Cities

 *[#lightgreen] ¿Qué exige\nla consultoria?
  *[#FFBBCC] Aptitud
   *[#lightblue] Buenos profesionales 
    *[#lightgreen] Formación y experiencia
    *[#lightgreen] Capacidad de trabajo y evolución
    *[#lightgreen] Colaborar en proyectos común
    *[#lightgreen] Trabajar en equipo  
    *[#lightgreen] Ofrecer honestidad y sinceridad     
  
  *[#FFBBCC] Actitud
   *[#lightblue] Productividad
    *[#lightgreen] Implica iniciativa y anticipación
    *[#lightgreen] Es imprescindible en consultoria
   *[#lightblue] Voluntad de Mejora
    *[#lightgreen] Actitud activa en construcción de empresa
    *[#lightgreen] Aportación de ideas de mejora
   *[#lightblue] Responsabilidad
    *[#lightgreen] Asumir los trabajos para hacerlos
    *[#lightgreen] No devolver nuevos problemas
    *[#lightgreen] No esgrimir razones para no llevar\na cabo el trabajo

  *[#FFBBCC] Compromiso
   *[#lightblue] Disponibilidad total
   *[#lightblue] Nuevos retos
   *[#lightblue] Viajes

 *[#lightgreen] ¿Cómo es la\ncarrera profesional?
  *[#FFBBCC] Modelo de RRHH
   *[#lightblue] Modelo de Categorías
   *[#lightblue] Plan de Carrera
   *[#lightblue] Proceso de Evolución
   *[#lightblue] Plan de Formación
  *[#FFBBCC] Categorías Profesionales
   *[#lightblue] Junior
    *[#lightgreen] Trabajo en Equipo y Gestión del Tiempo
    *[#lightgreen] Entrevistas Eficaces
    *[#lightgreen] Presentaciones Eficaces
   *[#lightblue] Senior
    *[#lightgreen] Motivación y Liderazgo
    *[#lightgreen] Técnicas Comerciales
   *[#lightblue] Gerente
    *[#lightgreen] Negociación
   *[#lightblue] Director

@endmindmap
```
# MAPA 2 - Consultoría de software.
```plantuml
@startmindmap
*[#Orange] Consultoría\nde software.
 *[#lightgreen] Desarrollo de\nSoftware
  *[#FFBBCC] Una de las actividades que se lleva a cabo en una consultora\ntecnológica es el desarrollo de software.
  *[#FFBBCC] Proceso para el desarrollo de software 
   *[#lightblue] Comienza cuando un cliente tiene una necesidad\ndentro de su negocio. 
    *[#lightgreen] Ejemplo (Llega un cliente y quiere vender un producto\nen el mercado, no tengo una aplicación que me permite\ncontrolar y almacenar los clientes, así que necesita realizarlas\ntareas diarias para almacenar la información y luego procesarla\ncon el fin de realizarlas de manera ágil y productivo).
   *[#lightblue] Consultoría: 
    *[#lightgreen] Consiste en hablar con el cliente y ver\nque requisitos requerimientos tiene que\ntener el softwareque se va a desarrollar.
   *[#lightblue] Estudio de viabilidad: 
    *[#lightgreen] Analizas lo que el cliente quiere y le muestras las ventajas\npuede ser de manera económica y que costo va a tener el software.\nAl final estudias con el cliente si es viable o si tiene sentido o no.
   *[#lightblue] Diseño funcional:
    *[#lightgreen] Consiste en ver qué información necesita el sistema de información de\nsoftware de entrada en el sistema, que información vas introducir en el sistema\ny que información nos va a mostrar el sistema.La información de entrada y salida\nes la que almacenamos en la base de datos.
    *[#lightgreen] Construye el modelo de datos que vamos a utilizar y además cuantos\nprocesos de entrada y de salida al igual que los procesos repetitivos\nva a tener el sistema. También se hace un prototipo con el fin que el\ncliente vea como estará el diseño del sistema.
   *[#lightblue] Diseño técnico: 
    *[#lightgreen] Se aísla el tipo de servicio de cliente o de negocio con la parte técnica.\nEl diseñador técnico va a trasladar la necesidad a nivel técnico mediante\nun lenguaje de programación y además en un entorno basado en hardware\nde tipo servidores de aplicaciones que se va a utilizar.
    *[#lightgreen] Su objetivo es crear todos los procesos considerando las necesidades\nde datos y luego los programadores construyen el software con el lenguaje\ndecidido por el diseñador técnico.
   *[#lightblue] Pruebas funcionales:
    *[#lightgreen] Cuando el software esta construido y que el técnico decide que\ncumple con las especificaciones técnicas, aquellos que hicieron\nlos requerimientos funcionales (“especialistas de negocio”) prueban\nque todos aquellos requerimientos que el cliente pidió se cumplen. 
    *[#lightgreen] Después de que la empresa decide que el producto cumple con todos los\nrequerimientos tanto técnicos y funcionales del cliente. El cliente lo recibe\ny se hacen las pruebas de usuario. Cuando el cliente esta satisfecho con el\nsistema, lo que procede será implantarlo a todos los ordenadores o servidores\ndonde el cliente los necesite. 

   *[#lightblue] Relaciones con el cliente\ny entrega de productos:
    *[#lightgreen] Un proyecto, existen metodologías de gestión de proyectos que son comunes\npara cualquier tipo de proyecto. 
    *[#lightgreen] El jefe de proyecto es la persona indicada de planificar las fases del\nproyecto y ver los recursos necesitan, al igual que hablar con el cliente\nconstantemente para definir los requisitos y para hacer pruebas. 
    *[#lightgreen] Depende que metodología quiere el cliente podría ser ágil, DevOps, tradicional. También\ninfluye el volumen del proyecto para poder elegir la metodología deseada.
    *[#lightgreen] ¿Como se materializa los procesos a través de\nproyecto típico o factorías de software?
     *[#lightgreen] El cliente tiene que decidir donde se hace el desarrollo del software se\nrealice en sus instalaciones “El jefe del proyecto decide cuales\nson las personas ideales para ir a sus instalaciones y desarrollar\nel software” o que se realice en las instalaciones de AXPE Consulting.
     *[#lightgreen] Lo más habitual o lo común es que el cliente decida que\nse realice en las factorías de software.
     *[#lightgreen] Si se logra separar el proyecto en tareas que se realiza en la mayoría de\nlos proyectos, es decir en departamentos de manera que se aísla la\nparte de diseño técnico y diseño funcional a partir de ahí crear\nlos “Cuadernos de carga “ 


 *[#lightgreen] Actividades desarrolladas\nen AXPE Consulting
  *[#FFBBCC] Es una empresa de consultoría informática, las empresas de consultoría realizan todas\naquellas tareas que permiten crear o mantener “informática de una empresa”. 
  *[#FFBBCC] ¿Qué es lo que necesita una empresa\npara poder funcionar y mantenerse?
   *[#lightblue] Lo primero es crear la parte de infraestructura (ordenadores,\nservidores donde se almacena toda la información, etc.).
   *[#lightblue] Habilitar toda la infraestructura de comunicaciones que nos permite\ncomunicarnos con el exterior (internet o líneas de comunicaciones) .
   *[#lightblue] Asegurarse que está protegida la información\nprincipalmente la de los clientes 
   *[#lightblue] Aplicaciones que permite a la empresa\ndesarrollarse se refiere al desarrollo del software.
  *[#FFBBCC] ¿Qué pasa si deja de funcionar\ny cómo hacerlo que vuelva a funcionar?
   *[#lightblue] Depende del volumen de la empresa si es grande o pequeña. 
    *[#lightgreen] En una empresa pequeña se puede que ellos mismos dándoles\nuna serie de mecanismos lo puedan resolver. 
    *[#lightgreen] En una gran empresa ejemplo un banco, necesita tenera un\nequipo de personas que están constantemente monitorizando\nlos procesos que se están lanzando. 
  *[#FFBBCC] Factor de escala
   *[#lightblue] La organización se va adecuando a la necesidad\ndel servicio, proyecto y del cliente. 
    *[#lightgreen] Dentro de la organización están oficinas de proyecto que\natienden a un gran cliente (que tenga muchos proyectos) se\ndebe tomar como una unidad global.  
    *[#lightgreen] En caso de clientes pequeños no se debe tener una\nestructura grande sino de una forma más personalizada.

 *[#lightgreen] Tendencias actuales de desarrollo\nen un sistema de información
  *[#FFBBCC] Las tendencias actuales empiezan por cambiar el concepto lo que\nantes era la infraestructura de una empresa, antes todas las empresas\ntenían sus propios ordenadores donde almacenaban la información. 
   *[#lightblue] Tenían que tener habilitado toda la parte de comunicaciones,\nseguridad, el software de infraestructura, etc. 
  *[#FFBBCC] Hoy en día se almacena la información en la NUBE(estar en\nservidores de grandes incorporaciones toda tu información. 
   *[#lightblue] Por ejemplo, Google, Amazon y empresas especializadas en información\nen la nube. Con el fin de que la seguridad lo realicen las empresas en este\ncaso Google te garantiza que tu información estará segura. 
  *[#FFBBCC] Antes se utilizaban ordenadores de almacenamiento limitado y\ncuando se acababa era necesario comprar otro ordenador. Lo que\nte permite este tipo de empresas es pagarles por consumo. 
   *[#lightblue] Ejemplo cuando una empresa quería hacer marketing o guardar una\ngran cantidad de información tenías que comprar un ordenador\nespecifico para esa tarea, en cambio con tu proveedor de la nube\nencargas un aumento de memoria durante un tiempo pagas por\nello y cuando ya no lo requieras mas dejas de pagarlo.
    *[#lightgreen] Este es el concepto de trabajo en la nube.
   *[#lightblue] Que inconvenientes 
    *[#lightgreen] En Google te tienes que regir a la ley Americana de los Estados\nUnidos porque es donde está ubicada todos sus servidores.
    *[#lightgreen] La seguridad de la información si hablamos de empresas con datos\nsensibles por ejemplo temas de seguridad de algún país\nevidentemente no tiene sentido subir toda la información a\ninfraestructuras donde no dependen de ti.


 *[#lightgreen] Visita técnica a una consultora\nde software: AXPE Consulting
  *[#lightblue] Es una organización nacida en la comunidad de Madrid que presta servicios\nde consultoría y desarrollan software avanzado a las mayores organizaciones\ndel país de España y de otros países. 
  *[#lightblue] Utilizan todas las técnicas y todos los métodos más avanzados para prestar\nservicios de mas alto valor a todas las organizaciones en cada sector de actividad.
  *[#lightblue] Su futuro como compañía esta orientado en el desarrollo de servicios de alto\nvalor tanto en la capacidad de servicios como en el uso de las tecnologías orientando\na los clientes al uso intensivo de productos y servicios para conseguir la máxima\neficiencia para sus clientes. 
   *[#lightgreen] Sus principales lugares de expansión son Latinoamérica y Europa.
@endmindmap
```
# MAPA 3 - Aplicación de la ingeniería de software.
```plantuml
@startmindmap
*[#Orange] Stratesys: Aplicación de\nla ingeniería de software.
 *[#lightgreen] Es una empresa de servicios de tecnología que llevan en el mercado 15 años,\n100% español. Son expertos en soluciones app.
 *[#lightgreen] SAP
  *[#FFBBCC] Dentro del mundo del software las empresas\ntienden a productos que sean comerciales\ny sobre todo a soluciones RP.
   *[#lightblue] Son soluciones que pretenden cubrir todos\nlos procesos de negocios que hay dentro de una empresa,\ndesde que se compra hasta que se vende y toda\nla parte financiera.
   *[#lightblue] Es un paquete de software que es configurable.\nEs una solución que cubre el estándar de ese proceso de negocio.\nCada uno va adaptando según a su sector. Con lo cual tiene\nuna capa que es la parametrización.
   *[#lightblue] Tiene un FRAMEWORK de desarrollo.\nEsta basado en un lenguaje ABAP


 *[#lightgreen] Enfoque metodológico
  *[#FFBBCC] Requisitos del cliente
   *[#lightblue] Lo que el cliente necesita 
   *[#lightblue] En qué áreas quiere aplicar la aplicación 
  *[#FFBBCC] Reunión con los jefes de cada área
   *[#lightblue] Director financiero, director de compras, director de ventas 
   *[#lightblue] Una visión global de toda la empresa.
   *[#lightblue] Procesos de la empresa
  *[#FFBBCC] Requisito del Análisis funcional
   *[#lightblue] Prácticamente describe lo que se necesita hacer
  *[#FFBBCC] Generación del Análisis funcional
   *[#lightblue] Determina como se realiza las tareas y en cuanto tiempo tardara
  *[#FFBBCC] Análisis técnico
   *[#lightblue] Programación
    *[#lightblue] Uso de las herramientas  
    *[#lightblue] Desarrollo de los programas
     *[#lightblue] Diseño técnico
     *[#lightblue] Definir variables
     *[#lightblue] Autónomo a las pruebas
     *[#lightblue] Integración con los procesos
   *[#lightblue] Parametrización

 *[#lightgreen] Desarrollo del proyecto
  *[#FFBBCC] Dividido en tres grandes fases
   *[#lightblue] Fase de análisis 20%
    *[#lightblue] Define y como se hará el proyecto
   *[#lightblue] Fase de construcción 60%
    *[#lightblue] La etapa de desarrollo con los requerimientos solicitados
   *[#lightblue] Fase de pruebas 20%
    *[#lightblue] Pruebas para la detección y corrección de errores

@endmindmap
